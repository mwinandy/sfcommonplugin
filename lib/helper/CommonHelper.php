<?php

/**
 * Permet d'entourer les lettre en majuscule d'un span
 * 
 * @param type $string
 * @return type
 */
function upperSpan($string)
{
    return preg_replace('/[A-Z]/', '<span class="uppercase">$0</span>', $string);
}

/**
 * Permet d'afficher une date en français, si $abbreviated = true, le jour de la semaine est abrégé.
 * 
 * @param timestamp $timestamp
 * @param bool $abbreviated
 * @return string
 */
function date_FR($timestamp, $abbreviated = false)
{
    if ($abbreviated)
    {
        $Jour = array("Dim.", "Lun.", "Mar.", "Mer.", "Jeu.", "Ven.", "Sam.");
        $Mois = array("Jan.", "Fév.", "Mars", "Avr.", "Mai", "Juin", "Juil.", "Aoû.", "Sept.", "Oct.", "Nov.", "Déc.");
    } else
    {
        $Jour = array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
        $Mois = array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
    }
    return $Jour[date("w", $timestamp)] . " " . date("d", $timestamp) . " " . $Mois[date("n", $timestamp) - 1] . " " . date("", $timestamp);
}

/**
 * Permet d'insérer le code de suivi Google Analytics.
 * 
 * @param string $account
 * @return string
 */
function googleAnalytics($account = false)
{
    if (sfConfig::get('app_google_analytics', false)) :
        ob_start();
        ?>
        <script type = "text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', '<?php print $account ? $account : sfConfig::get('app_google_analytics', null)  ?>']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
        <?php
        return ob_get_clean();
    endif;
}

/**
 * Crée un arbre html <ul><li></li><ul> à multiple niveaux.
 * 
 * @todo Ajouter la gestion de la limite de profondeur $depth
 * @todo Ajouter la gestion du niveau html $current_depth
 * @todo Ajotuer la gestion du noeud courrant $current_node basé sur l'url ?
 * 
 * @param array $nodes Un tableau association #LABEL# => #URL#
 * @param bool $isRoot Internal parameter
 * @return string
 */
function menu($nodes, $isRoot = true, $class = "sf-menu")
{
    ob_start();
    if (count($nodes, true) > 0):
        ?>
        <ul<?php echo $isRoot ? ' class="' . $class . '"' : null; ?>>
                <?php foreach ($nodes as $label => $url): ?>
                <li>
                    <?php if (is_array($url)): ?>
                        <a href="#"><?php echo $label ?></a>
                        <?php echo menu($url, false); ?>
                    <?php else: ?>
                        <a href="<?php echo $url ?>"><?php echo $label ?></a>
                <?php endif; ?>
                </li>
        <?php endforeach; ?>
        </ul>
        <?php
    endif;
    return trim(preg_replace('/\s+/', ' ', ob_get_clean()));
}

/**
 * Permet le tri sur un tableau multi-dimensionnel.
 * 
 * @param pointer $array Le tableau à trier
 * @param string $key La clé sur laquelle basé le tri
 */
function aasort(&$array, $key)
{
    $sorter = array();
    $ret = array();
    reset($array);
    foreach ($array as $ii => $va)
    {
        $sorter[$ii] = $va[$key];
    }
    asort($sorter);
    foreach ($sorter as $ii => $va)
    {
        $ret[$ii] = $array[$ii];
    }
    $array = $ret;
}

function arrayTree($nodes, $labelField, $route, $routeParameter, $orderField = false, $forceATag = false)
{

    if ($orderField)
    {
        aasort($nodes, $orderField);
    }

    $r_array = array();

    if (count($nodes) > 0)
    {
        foreach ($nodes as $node)
        {
            if (count($node['__children']) > 0)
            {
//$r_array[$node[$labelField]] = '#';
                $r_array[$node[$labelField]] = arrayTree($node['__children'], $labelField, $route, $routeParameter, $orderField, $forceATag);
            } else
            {
                $r_array[$node[$labelField]] = url_for(sprintf($route, $node[$routeParameter]));
            }
        }
    }

    return $r_array;
}

function htmlTree($nodes, $labelField = null)
{
    if (count($nodes) > 0)
    {

        echo '<ul>';

        foreach ($nodes as $node)
        {

            echo '<li>';

            echo $node[$labelField];

            htmlTree($node['__children'], $labelField);
            echo '</li>';
        }

        echo '</ul>';
    }
}

function gritter()
{

    if (sfContext::getInstance()->getUser()->hasFlash('growl')):
        ob_start();
        ?>
        <script type="text/javascript">
        <?php foreach (sfContext::getInstance()->getUser()->getFlash('growl') as $notification): ?>
                                                                                                                                                                                                                                                                                                                                                          
                $.gritter.add({
                    title: <?php echo json_encode($notification['title']) ?>,
                    text: <?php echo json_encode($notification['text']) ?>,
                    image: <?php echo $notification['image'] ? json_encode(image_path($notification['image'])) : 'false'; ?>,
                    time: 5000,
                    sticky: false
                });

        <?php endforeach; ?>
        </script>
        <?php
        sfContext::getInstance()->getUser()->setFlash('growl', null);
        return ob_get_clean();
    endif;
}

function tags($tags, $separator = ',', $icons_class = 'icons-tag')
{
    if (!is_array($tags))
    {
        $tags = explode($separator, $tags);
    }
    ob_start();
    foreach ($tags as $tag)
    {
        echo sprintf('<span class="tag"><span class="%s">&nbsp;</span>%s</span>', $icons_class, $tag);
    }
    return ob_get_clean();
}