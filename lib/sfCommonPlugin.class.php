<?php

/**
 * sfCommonPlugin class
 * 
 * @package    sfCommonPlugin
 * @author     Maxence Winandy <maxence.winandy@gmail.com>
 */
class sfCommonPlugin
{

  public static function addGrowlNotification($title, $text, $image = false)
  {
    $array = sfContext::getInstance()->getUser()->getFlash('growl', array());
    $array[] = array(
        'title' => $title,
        'text' => $text,
        'image' => $image,
    );
    sfContext::getInstance()->getUser()->setFlash('growl', $array);
  }

  public static function getGrowlNotifications()
  {
    return sfContext::getInstance()->getUser()->getFlash('growl');
  }

  public static function getGrowlImage($image = false)
  {
    sfApplicationConfiguration::getActive()->loadHelpers(array('Common', 'Asset'));
    return image_path(sprintf('growl/%s', $image ? $image : 'empty'), true);
  }

}