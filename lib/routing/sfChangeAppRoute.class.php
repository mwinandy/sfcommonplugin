<?php
 
class sfChangeAppRoute extends sfRoute{
 
    public function generate($params, $context = array(), $absolute = true)
  {
 
    if(!array_key_exists('app',$params))
        return parent::generate($params, $context, $absolute);
 
 
    $app_name=sfContext::getInstance()->getConfiguration()->getApplication();
    $env_name=sfContext::getInstance()->getConfiguration()->getEnvironment();
    if($env_name=='prod')
        $env='';
    else
        $env='_'.$env_name;
 
    $url='/../'.$params['app'].$env.'.php';
 
    return $url;
  }
 
}
 
?>